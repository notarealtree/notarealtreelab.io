---
title: Learning how to make better pizza
date: '2019-01-07T23:07:00.000Z'
---

**TL;DR:** Using a pizza peel makes making pizza easier, recipe below.

Some time in 2018, my wife and I bought a pizza stone with the intention of home-making pizza
to both reduce the number of takeouts we order, as well as have a new meal to add to our regular
home-cooking rotation.

We tried making 'proper' pizza a couple of times with strongly mixed results. Sometimes the base
would end up undercooked or downright sludgy, toppings would fall off or slide to one side of the
pizza when taking the pizza out of the oven, and lots of other frustrating things would happen
in the process.

At first I was under the impression we might be using the wrong dough-recipe, so we switched that up,
but to no avail. Next I thought the lack of using semolina flour (to keep the pizza from sticking to
whichever surface we were working on was the issue), but even adding that helped nothing. It seemed
that the taste was usually alright, but the process of getting there was so frustrating that we
stopped making pizza.

Late in the year it occurred to me that our actual problem might be with the process, rather than the
lack of ingredients so I bought a pizza paddle (or peel, I'm not really sure) to see if this would
help. In conjunction with this I went specifically looking for a recipe that would produce 'crispy'
pizza, rather than the soggy dough soup bowls filled with melted cheese that go as artisanal pizza in London.

I found a promising recipe [here](https://www.yuppiechef.com/spatula/pizza-recipe/) and went to try it out.
For my first try, I rolled out the dough on the countertop, added the toppings and then tried to get
the unbaked pizza onto the pizza peel. This was a complete catastrophe and I ended up surrendering that
attempt.

For my next try, I rolled the dough out on the pizza peel (making sure it was well floured) and this ended up
working fairly well. It stuck a little bit but I realized that by occasionally moving the pizza peel back
and forth in a quick motion, I could keep the pizza from sticking. I managed to get my pizza onto the pizza
stone in the oven without any further catastrophes and it turned out pretty good. I've made the whole
thing again since, and figured out the correct cooking time for my stone/oven.

The recipe I stuck with is as follows (2 pizzas for roughly 2 adults):

```
250g All Purpose flour
2 tbsp Extra Virgin Olive Oil
125ml Lukewarm Water
8g Fast Action Yeast (just what my packet size worked out to be)
A Pinch of Salt
```

I combined them all into a dough and used my trusty kitchenaid with a dough hook to knead the dough for
about 10 minutes. I then took the ball of dough and put it into a pre-oiled bowl, covered the bowl with a
kitchen towel and let it sit for an hour. In the meantime I heated up the oven to 250°C (with the stone
inside for obvious reasons).

For toppings for each pizza I used
```
1/4 Can of passata
7 Slices of Milano Salami (thin means crispy)
1/2 Ball of Mozzarella cut into thin slices
Rocket (for topping after it comes out of the oven)
```

After an hour had passed, I rolled out the dough on the pizza peel, making sure to keep it moving periodically,
folded over about 2cm worth of edge to make a raised crust, spread the passata and salami on the non-crust surface
of the dough, and applied some olive oil to the crust with the back side of a spoon, which apparently makes
for more crisp.

I then baked the pizza for about 10 minutes, applied some Herbes de Provence, freshly ground pepper and the rocket,
and ate it. It was pretty good, though I'm sure I'll improve with more practice. Improving the crust is my next goal,
it can end up kind of dense.

