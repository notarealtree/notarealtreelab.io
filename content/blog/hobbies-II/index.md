---
title: Hobbies II
date: '2019-03-04T20:00:00.000Z'
---

Hobbies II

This past week I played my first game of Warhammer 40K since around about 2005 (I can't remember the actual year but it must have been some time around then). The game is very different from what I remembered but I had a fantastic time, not least because the person I was playing against did their best in helping me out with lacks of rule knowledge and such. I have also gained an understanding of what a 'Smash Captain' is, as well as why I hate them.

Getting to this point was quite a slog, it took me about 2 months of assembly and painting to get to 1000 points (a small but playable force) in order to get in a game with a sensible amount of models on the table. Having assembled and painted this amount motivates me to do more, and I have a reasonable amount of unpainted or partially painted stuff ready to go. I've found painting to be a nice counter balance to my daily work, as it actually requires fine motor skills and comparing models I paint now to models I painted in the past gives a good indication of the amount of skill I've gained as a painter (and the amount of liquid skill I now know to apply, thank you Games Workshop).

Investing all this time in painting has meant a decrease in the amount of time I've played video games for, although given the recent selection of video games there's nothing massively captivating available anyways. My friends and I have been playing some Anthem recently, but I find its story uninteresting and bland, I only really play it for the mechanics and the gameplay (which I do enjoy). I saw Space Engineers released out of Early Access recently so maybe I'll give that another go, I've always enjoyed playing Space Engineers (until I eventually burn out). The new standalone expansion of Subnautica looks promising and I'm tempted to pick it up (even in early access) given that Subnautica was my favourite game of 2018.

Having just finished my 'Centerpiece', Belisarius Cawl, I look forward to working on some Armiger Warglaives next, the flair of knightly courts has been very attractive to me since listening to 'Master of Mankind' and I think they fit in quite well with the whole Adeptus Mechanicus flair.
