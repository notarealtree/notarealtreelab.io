---
title: Tree time
date: '2019-01-07T21:06:00.000Z'
---

I decided to start writing some things down today. I don't think I'm really a diary type
person and sparked by this [reddit post](https://old.reddit.com/r/programming/comments/adbu86/why_medium_is_no_longer_the_goto_platform_for/)
I decided to go for writing on a blog instead. I've never written long-form posts before and I'm
not sure that that's what I want to do here either.

I don't plan on having a specific topic, release schedule or anything else for this, I'd much rather
just write about things I find interesting when I am in the mood to do so.

If you find yourself reading this and find any of my posts interesting or entertaining, I'm happy to have done so.

I'm not sure if gatsby supports comments but I'm honestly not that interested in having them, if you do feel
very compelled to get in touch with me, you could file an issue on the gitlab repo this is based on.