---
title: Cats
date: '2019-01-08T23:50:00.000Z'
---

I have a cat. Well, technically, he's my wife's cat but we have some sort of "What's yours is mine" thing going on so I guess he's really my cat as well. He moved in with us in 2017 and has been plaguing us, and making us happy at the same time, since then (and my wife before that as well).

I kind of like having a cat, it's nice not to be alone, even when my wife is out, and it's nice always having someone to welcome you home (even if it's probably more out of desire for food). It's also nice having a warm and fuzzy creature to cuddle with and I know he provides lots of emotional support to both my wife and me. I never really thought I was much of a pet person before but I get the appeal now, though I don't think I could deal with anything that requires much more attention than our cat.

In general, he's quite nice to get along with though he can be incredibly infuriating in the mornings, when he wakes us up an hour early to get his food, before he then proceeds to sleep the whole day.

I'm not sure I could put up with 2 cats, having a litter box kind of bums me out and having more of those wouldn't really improve this. Going forwards however I envision having at least exactly one cat, having him around is a net positive on our household, even if we have to literally clean up after him repeatedly. He's also cute as hell.

![Karl](./karl.jpg)