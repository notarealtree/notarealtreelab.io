---
title: Hobbies
date: '2019-01-15T20:46:00.000Z'
---

Sometimes I think I might have too many hobbies. While I'm not sure that that's strictly an issue (after all switching the things you do in your spare time seems like a healthy thing to do), sometimes I wonder whether I should consolidate a little bit. At the moment, things I consider hobbies that I do more or less regularly are:

- Play video games - I do this quite a lot actually.
- Home brewing - This is something that involves a lot of waiting with occasional periods of high-activity. I enjoy it immensely and the 'fruits' of my labour are quite tasty, most of the time.
- Painting miniatures - I've gotten back into this recently, after having done it as a teenager. It's a lot of fun and I can't wait to use the army I'm painting up (Warhammer 40K) on the tabletop.
- Tinkering with electronics - I bought a Raspberry Pi and a kit with a bunch of sensors recently and started playing around with that. While I'm not sure how regular a thing this will be long-term, I enjoyed assembling small things that did something.
- Writing Code - I do this for work but I also enjoy doing it outside of work and sometimes do it in my free time. Recently I've done less of this, and I actually think I will stop doing it outside of work hours as I find it exacerbates symptoms of burnout that I occasionally feel I have.
- Playing Guitar - I picked up a guitar last year with the intention of picking up playing an instrument again. I've been having a hard time motivating myself to do it now but feel that I don't want to get rid of it for the moment.

All these things bring me enjoyment, though some have done more consistently than others. If I had to pick something to get rid of at this particular point in time it would probably be the electronics bit, I'm not sure of the long-term enjoyment prospects of that.

Ultimately, I think as long as something you do outside of your work hours brings you enjoyment and relaxation it's a worthwhile pursuit (assuming it doesn't harm anyone or breaks any laws).

I've found an odd number of people who, at least as far as they're concerned, don't have any hobbies. Some of them actually do have hobbies but just don't look at them as such, but others genuinely don't seem to do much outside of work hours other than more work, or watch netflix. I find this quite bizarre, given that I mostly want to do something creative, or productive, rather than just sit and binge-watch media content, but I feel it's not my place to judge whether what they do is a worthy way of spending their time.